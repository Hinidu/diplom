RESULT_FILE_NAME = diplom
PDF_LATEX = pdflatex -shell-escape

all: $(RESULT_FILE_NAME).pdf

diplom.pdf: *.tex
	latexmk -pdf -pdflatex="$(PDF_LATEX)" -jobname=$(RESULT_FILE_NAME) main.tex

clean:
	latexmk -C -jobname=$(RESULT_FILE_NAME) main.tex
